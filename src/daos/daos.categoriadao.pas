unit daos.categoriadao;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  sqldb,
  pqconnection,
  models.categoria;

type
  TCategoriaDAO = class
    private
      { privates declarations }
      fConexao: TPQConnection;
      fQuery: TSQLQuery;
    public
      { public declarations }
      constructor create(var conexao : TPQConnection);
      function save(model : TCategoriaModel) : boolean;
      function find(id : integer) : TCategoriaModel;
      function edit(model : TCategoriaModel) : boolean;
      function delete(id : integer) : boolean;

      { properties declarations }
      property Conexao : TPQConnection read fConexao write fConexao;
      property Query : TSQLQuery read fQuery write fQuery;
  end;

implementation

{ Construtor de classe }
constructor TCategoriaDAO.create(var conexao : TPQConnection);
begin
  self.fQuery := TSQLQuery.create(Nil);

  self.fConexao := conexao;
  self.fQuery.SQLConnection := self.fConexao;
end;

{ save }
function TCategoriaDAO.save(model : TCategoriaModel): boolean;
begin

  Result := True;
end;

{ find }
function TCategoriaDAO.find(id : integer): TCategoriaModel;
var
  model: TCategoriaModel;
begin

  model := TCategoriaModel.create;
  Result := model;
end;

{ edit }
function TCategoriaDAO.edit(model : TCategoriaModel): boolean;
begin

  Result := True;
end;

{ delete}
function TCategoriaDAO.delete(id : integer): boolean;
begin

  Result := True;
end;

end.

