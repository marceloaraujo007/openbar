unit views.categorias;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, DBGrids,
  ComCtrls, Buttons, ExtCtrls, StdCtrls, MaskEdit, Grids;

type

  { TfrmCategorias }

  TfrmCategorias = class(TForm)
    btnExcluir: TBitBtn;
    btnCadastrar: TBitBtn;
    btnCancelar: TBitBtn;
    btnPesquisar: TBitBtn;
    cbStatus: TComboBox;
    dbgridCategorias: TDBGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    lbStatus: TLabel;
    lbedNome: TLabeledEdit;
    lbedPesquisa: TLabeledEdit;
    medCriado: TMaskEdit;
    medAtualizado: TMaskEdit;
    PageControl2: TPageControl;
    rbAtivo: TRadioButton;
    rbInativo: TRadioButton;
    stCriado: TStaticText;
    stAtualizado: TStaticText;
    stStatus: TStaticText;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure dbgridCategoriasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnCadastrarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure TabSheet1Show(Sender: TObject);
    procedure TabSheet2Show(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmCategorias: TfrmCategorias;

implementation

{$R *.lfm}

uses
  models.categoria,
  daos.categoriadao,
  datamodules.conexao;

{ TfrmCategorias }

procedure TfrmCategorias.FormCreate(Sender: TObject);
begin

end;

procedure TfrmCategorias.dbgridCategoriasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
   {TODO -oRodrigoSoares -cGrid: Mudar texto 'ATIVO'/'INATIVO' por seus respectivos ícones}
end;

procedure TfrmCategorias.btnCadastrarClick(Sender: TObject);
var
  query: string;
  nome : string;
  status: string;
  categoria: TCategoriaModel;
  categoriaDAO: TCategoriaDAO;
begin
  { Criar lógica para INSERT }
  nome := lbedNome.Text;
  if rbAtivo.Checked then
  begin
    status := 'ATIVO';
  end
  else
  begin
    status := 'INATIVO';
  end;

  { Se ouver valores escritos, monsta-a query de inserção }
  if (nome <> '') then
  begin
    {TODO -oRodrigoSoares -cDAO: Mover essa lógica para o metodo save() da classe DAO}
    { Instanciando objetos necessários para manipular banco de dados }
    categoria := TCategoriaModel.create;
    categoriaDAO := TCategoriaDAO.create(dmConexaoBanco.conexaoBanco);

    query := 'INSERT INTO categorias(nome, status, criado_em, atualizado_em) values(:nome, :status, :criado_em, :atualizado_em);';

    categoriaDAO.Query.SQL.Text := query;
    categoriaDAO.Query.Params.ParamByName('nome').AsString := nome;
    categoriaDAO.Query.Params.ParamByName('status').AsString := status;
    categoriaDAO.Query.Params.ParamByName('criado_em').AsDate := Date;
    categoriaDAO.Query.Params.ParamByName('atualizado_em').AsDate := Date;

    categoriaDAO.Query.ExecSQL;
    categoriaDAO.Conexao.Transaction.Commit;

    { Nessário reabrir TSQLQuery }
    dmConexaoBanco.qryCategorias.Open;

    ShowMessage('Cadastro realizado com sucesso!');
  end
  else
    ShowMessage('Valores incorretos. Repita a operação.');
  begin

  end;

end;

procedure TfrmCategorias.btnCancelarClick(Sender: TObject);
begin
  self.Close;
end;

procedure TfrmCategorias.btnPesquisarClick(Sender: TObject);
var
  query: string;
begin
  { Criar lógica para pesquisa }
  { Dica: método Filter do TSQLQuery }
  ShowMessage('Falta implementar');
end;

procedure TfrmCategorias.TabSheet1Show(Sender: TObject);
begin
  self.btnCadastrar.Enabled := True;
  self.btnExcluir.Enabled := True;
end;

procedure TfrmCategorias.TabSheet2Show(Sender: TObject);
begin
  self.btnCadastrar.Enabled := False;
  self.btnExcluir.Enabled := False;
end;

end.

