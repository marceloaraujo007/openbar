unit views.principal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  ActnList, ComCtrls, EditBtn;

type

  { TfrmPrincipal }

  TfrmPrincipal = class(TForm)
    aCategorias: TAction;
    aProdutos: TAction;
    alMenuPrincipal: TActionList;
    smiCategorias: TMenuItem;
    smiProdutos: TMenuItem;
    miOpCaixa: TMenuItem;
    miCadastros: TMenuItem;
    miAjuda: TMenuItem;
    miVendas: TMenuItem;
    miMesas: TMenuItem;
    menuPrincipal: TMainMenu;
    StatusBar1: TStatusBar;
    procedure aCategoriasExecute(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.lfm}

uses
  views.categorias;

{ TfrmPrincipal }

procedure TfrmPrincipal.aCategoriasExecute(Sender: TObject);
begin
  frmCategorias.ShowModal();
end;

end.

