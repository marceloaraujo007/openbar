{ Arquivo de modelo da tabela Categorias }

unit models.categoria;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils;

type
  TCategoriaModel = class
    private
      { private declarations }
      fID: integer;
      fNome: string;
      fStatus: string;
      fCriadoEm: string;
      fAtualizadoEm: string;
      
      function getID(): integer;
      function getNome(): string;
      function getStatus(): string;
      function getCriadoEm(): string;
      function getAtualizadoEm(): string;

      procedure setID(id:integer);
      procedure setNome(nome:string);
      procedure setStatus(status:string);
      procedure setCriadoEm(data:string);
      procedure setAtualizadoEm(data:string);

    public
      { public declarations }

      { property declations}
      property ID: integer read getID write setID;
      property Nome: string read getNome write setNome;
      property Status: string read getStatus write setStatus;
      property CriadoEm: string read getCriadoEm write setCriadoEm;
      property AtualizadoEm: string read getAtualizadoEm write setAtualizadoEm;

  end;

implementation

function TCategoriaModel.getID():integer;
begin
  Result := self.fID;
end;

function TCategoriaModel.getNome(): string;
begin
  Result := self.fNome;
end;

function TCategoriaModel.getStatus(): string;
begin
  Result := self.fStatus
end;

function TCategoriaModel.getCriadoEm(): string;
begin
  Result := self.fCriadoEm
end;

function TCategoriaModel.getAtualizadoEm(): string;
begin
  Result := self.fAtualizadoEm
end;

procedure TCategoriaModel.setID(ID:integer);
begin
  self.fID := ID
end;

procedure TCategoriaModel.setNome(nome:string);
begin
  self.fNome := nome
end;

procedure TCategoriaModel.setStatus(status:string);
begin
  self.fStatus := status;
end;

procedure TCategoriaModel.setCriadoEm(data:string);
begin
  self.fCriadoEm := data;
end;

procedure TCategoriaModel.setAtualizadoEm(data:string);
begin
  self.fAtualizadoEm := data;
end;

end.
