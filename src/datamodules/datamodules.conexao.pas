unit datamodules.conexao;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, pqconnection, sqldb, db, FileUtil;

type

  { TdmConexaoBanco }

  TdmConexaoBanco = class(TDataModule)
    dsCategorias: TDataSource;
    conexaoBanco: TPQConnection;
    qryCategorias: TSQLQuery;
    trCategorias: TSQLTransaction;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  dmConexaoBanco: TdmConexaoBanco;

implementation

{$R *.lfm}

end.

