program openbar;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, views.principal, datamodules.conexao, views.categorias, 
daos.categoriadao
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TdmConexaoBanco, dmConexaoBanco);
  Application.CreateForm(TfrmCategorias, frmCategorias);
  Application.Run;
end.

